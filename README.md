# TeaCode React.js Recruitment Challenge

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).


## Installation and Setup Instructions

Clone down this repository. You will need node and npm installed globally on your machine.


### Installation:

```bash
npm install
```


### To Start Server:

```bash
npm start
```


### To Visit App:

```bash
localhost:3000
```



