import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import styled from 'styled-components';

const SpinnerWrapper = styled.div`
    display: flex;
    justify-content: center;
    margin-top: 2rem;
    height: 50px;
    
    .svq {
        width: 100px;
    }
`;

const Loading = () => (
    <SpinnerWrapper>
        <FontAwesomeIcon icon={faSpinner} className="fa-spin" />
    </SpinnerWrapper>
);

export default Loading;