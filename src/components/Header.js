import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.header`
    display: flex;
    justify-content: center;
    background-color: #d8e7ed;
    color: #333;
`;

const Header = () => (
    <Wrapper>
        <h1>Contacts</h1>
    </Wrapper>
);

export default Header;