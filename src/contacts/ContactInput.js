import React from 'react';
import styled from 'styled-components';

const InputWrapper = styled.div`
    padding: 1rem;

    > label {
        > input {
            margin-left: .7rem;
        }
    }
`;

const ContactInput = (props) => (
    <InputWrapper>
        <label>
            Input first name or last name: 
            <input 
                type="text"
                value={props.value}
                onChange={props.change} />
        </label>
    </InputWrapper>
);

export default ContactInput;