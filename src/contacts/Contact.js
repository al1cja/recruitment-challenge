import React from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser } from '@fortawesome/free-solid-svg-icons'

const ContactItem = styled.div`
    display: grid;
    grid-template-columns:  50px 50% auto;
    align-items: center;
    border: 1px solid #ccc;
    box-shadow: 0 0 2rem #ddd;
    width: 90%;
    margin: .1rem 0;

    &:hover {
        cursor: pointer;
        background-color: #eef4f7
    }

    div {
        margin-left: 1rem;
    }

    div.avatar {
        margin: 0;
        width: 50px;
        height: 50px;
        display: flex;
        align-items: center;
        justify-content: center;
    }

    span {
        margin: 0 .1rem;
    }

    >input {
        margin-right: 2rem;
        justify-self: flex-end;
    }
`

const Contact = ( props ) => {
    const avatar = props.contactData.avatar
        ? <img alt="" src={props.contactData.avatar} />
        : <FontAwesomeIcon icon={faUser} />;

    return (
        <ContactItem onClick={() => props.click(props.contactData.id)}>
            <div className="avatar">{avatar}</div>
            <div>
                <span>{props.contactData.first_name}</span>
                <span>{props.contactData.last_name}</span>
            </div>
            <input
                checked={props.isChecked}
                onChange={() => props.click(props.contactData.id)}
                type="checkbox" />
        </ContactItem>
    );
}

export default Contact;