import axios from 'axios';
import React, { useState, useEffect } from 'react';
import Contact from './Contact';
import ContactInput from './ContactInput';
import Loading from '../components/Loading';
import styled from 'styled-components';

const ContentWrapper = styled.div`
    display: flex;
    flex-flow: column;
    align-items: center;
`;

const Contacts = () => {
    const [isLoading, setIsLoading] = useState(true);
    const [contactsData, setContactsData] = useState();
    const [checkData, setCheckData] = useState([]);
    const [searchQuery, setSearchQuery] = useState([]);

    useEffect(() => {
        axios.get('/users.json')
            .then((response) => {
                let newData = [...response.data];
                let newCheck = [];
                newData.forEach(entry => {
                    newCheck.push(false);
                })
                setCheckData(newCheck);
                setContactsData(newData);
                setIsLoading(false);
            })
    }, []);

    const handleClickContact = (id) => {
        let newCheckData = [...checkData];
        newCheckData[id] = !newCheckData[id];
        setCheckData(newCheckData);
        let checkedContacts = 
            contactsData.filter(obj => newCheckData[obj.id])
            .map(obj => obj.id)
        console.log("Checked contacts id:", checkedContacts);
    }

    const handleSearch = (e) => {
        setSearchQuery(e.target.value);
    }

    const content = isLoading 
        ? <Loading />
        : contactsData
            .filter(contact => 
                contact.first_name.toLowerCase().includes(searchQuery)
                || contact.last_name.toLowerCase().includes(searchQuery)) 
            .sort((a, b) => a.last_name.localeCompare(b.last_name))
            .map(contact => 
                <Contact
                    key={contact.id}
                    contactData={contact}
                    click={handleClickContact}
                    isChecked={checkData[contact.id]} />
        )

    const input = isLoading 
        ? null
        : <ContactInput
            value={searchQuery}
            change={handleSearch}/>

    return (
        <ContentWrapper>
            {input}
            {content}
        </ContentWrapper>
    )
};

export default Contacts;