import Contacts from './contacts/Contacts';
import Header from './components/Header';

function App() {
  return (
    <div className="App">
      <Header />
      <Contacts />
    </div>
  );
}

export default App;
